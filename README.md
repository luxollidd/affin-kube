Setup
-------
1. `composer install`
2. `npm install`
3. `npm run prod`
4. set the language in Settings page, currently please set Malay, English and Chinese
5. set the language in config > languages.php as follow
```php
<?php
return [
    'en' => 'English',
    'ms' => 'Malay',
    'zh' => 'Chinese',
];

```
6. set the domain in config > pagebuilder.php as follow
```php
    'domains' => [
        'localhost:8000' => 'master',
        'localhost:8001' => 'secondary',
    ];
```
currently only two layouts, and only master layout is working
7. `php artisan migrate && php artisan db:seed`

Usage
------
1. create a new page
2. set the details as requested in the create new page 
3. click on the edit button to go to page builder
4. drag and drop the blocks and edit accordingly
5. click save
6. view button is only work when it is saved
7. you may also view the page for public if the page is set to approved and within the publishing period
