<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyAttributesOnPageInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('page_info', function (Blueprint $table) {
            $table->string('status')->default('draft')->change();
            $table->dateTime('published_at')->default(now())->change();
            $table->dateTime('unpublished_at')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('page_info', function (Blueprint $table) {
            $table->string('status')->change();
            $table->dateTime('published_at')->change();
            $table->dateTime('unpublished_at')->change();
        });
    }
}
