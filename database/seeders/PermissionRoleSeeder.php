<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $permissionList = collect([
            'create-user',
            'read-user',
            'update-user',
            'delete-user',
            'create-role',
            'read-role',
            'update-role',
            'delete-role',
            'assign-role',
            'create-page',
            'read-page',
            'update-page',
            'delete-page',
            'publish-page',
            'unpublish-page',
            'create-page-on-specific-domain',
            'update-page-on-specific-domain',
            'delete-page-on-specific-domain',
            'create-domain',
            'read-domain',
            'update-domain',
            'delete-domain',
            'publish-domain',
            'unpublish-domain',
            'update-page-status',
            'update-published-date',
            'update-unpublished-date'
        ]);

        $permissionList->each(function ($permission) {
            Permission::findOrCreate($permission);
        });

        $roleMember = Role::firstOrCreate(['name' => 'member']);
        $roleMember->givePermissionTo('read-user');
        $roleMember->givePermissionTo('read-page');

        $roleSuperAdmin = Role::firstOrCreate(['name' => 'super-admin']);
        $roleSuperAdmin->syncPermissions($permissionList);

        $this->createUser('Super Admin', 'superadmin@cms.com', $roleSuperAdmin);
    }

    public function createUser($name, $email, $role)
    {
        $users = User::where('email', $email)->get();

        if (!sizeOf($users)) {
            $user = User::factory()->create([
                'name' => $name,
                'email' => $email
            ]);
            $user->assignRole($role);
        }
    }
}
