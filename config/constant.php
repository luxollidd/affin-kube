<?php

return [
    'STATUS' => [
        'draft' => 'Draft',
        'reviewing' => 'Reviewing',
        'approved' => 'Approved',
        'rejected' => 'Rejected'
    ]
];
