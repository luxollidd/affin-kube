<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'description' => 'Description',
    'twitter_creator' => 'Twitter Creator',
    'twitter_card' => 'Twitter Card',
    'twitter_site' => 'Twitter Site',
    'twitter_title' => 'Twitter Title',
    'twitter_description' => 'Twitter Description',
    'twitter_image' => 'Twitter Image',
    'twitter_image_alt' => 'Twitter Image Alt',
    'og_title' => 'Facebook Title',
    'og_type' => 'Facebook Type',
    'og_url' => 'Facebook Url',
    'og_image' => 'Facebook Image',
    'og_description' => 'Facebook Description',
    'og_locale' => 'Facebook Locale',
    'fb_app_id' => 'Facebook App ID',

];
