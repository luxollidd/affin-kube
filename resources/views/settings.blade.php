@extends('layouts.app')
@section('title')
    Edit Page
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading m-0">Edit Page</h3>
            <div class="filter-container section-header-breadcrumb row justify-content-md-end">
                <a href="{{ route('pages.index') }}"  class="btn btn-primary">Back</a>
            </div>
        </div>
        <div class="content">
            @include('stisla-templates::common.errors')
            <div class="section-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body ">
                                <?php
                                    $setting = phpb_instance('setting');
                                ?>

                                <form method="post" action="{{ route('settings.update') }}">
                                    @csrf
                                    <div class="main-spacing">
                                        <?php
                                        if (phpb_flash('message')):
                                        ?>
                                        <div class="alert alert-<?= phpb_flash('message-type') ?>">
                                            <?= phpb_flash('message') ?>
                                        </div>
                                        <?php
                                        endif;
                                        ?>

                                            {!! Form::label('languages', 'Languages:') !!}
                                            <select name="languages[]" id="languages" class="form-control select2" required multiple>
                                                @foreach (phpb_trans('languages') as $locale => $localeText)
                                                    <option
                                                        @if(phpb_e($setting::has('languages', $locale)))
                                                        selected
                                                        @endif
                                                        value="{{phpb_e($locale)}}">{{phpb_e($localeText)}}</option>
                                                @endforeach
                                            </select>

                                        <hr class="mb-3">

                                        <button class="btn btn-primary btn-sm">
                                            <?= phpb_trans('website-manager.save-settings'); ?>
                                        </button>
                                    </div>

{{--                                    <div class="main-spacing mt-5">--}}
{{--                                        <label class="d-block">--}}
{{--                                            <?= phpb_trans('website-manager.pagebuilder-block-images') ?>--}}
{{--                                        </label>--}}
{{--                                        <a href="<?= phpb_url('website_manager', ['route' => 'settings', 'action' => 'renderBlockThumbs']) ?>" class="btn btn-secondary btn-sm mr-1">--}}
{{--                                            <?= phpb_trans('website-manager.render-thumbs') ?>--}}
{{--                                        </a>--}}
{{--                                    </div>--}}

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


