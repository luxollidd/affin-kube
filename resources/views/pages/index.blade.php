@extends('layouts.app')
@section('title')
    Pages
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Pages</h1>
            <div class="section-header-breadcrumb">
                @can('create-page')
                    <a href="{{ route('pages.create')}}" class="btn btn-primary form-btn">Page <i
                            class="fas fa-plus"></i></a>
                @endcan
            </div>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    @include('pages.table')
                </div>
            </div>
        </div>

        {{--        @include('stisla-templates::common.paginate', ['records' => $pages])--}}

    </section>
@endsection

