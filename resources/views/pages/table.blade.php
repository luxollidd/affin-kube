<link href="{{ asset('/assets/css/jquery.dataTables.min.css') }}" rel="stylesheet">

<div class="table-responsive">
    <table class="table table-bordered" id="pages-table">
        <thead>
        <tr>
            <th class="text-center">Name</th>
            <th class="text-center">Route</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pages as $page)
            <tr>
                <td class="text-center">{{ $page->getName() }}</td>
                <td class="text-center">{{ $page->getRoute() }}</td>
                <td class="text-center">
                    {!! Form::open(['route' => ['pages.destroy', $page->get('id')], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('read-page')
                            <a href="{!! route('pages.show', [$page->get('id')]) !!}" target="_blank" class='btn btn-light action-btn '><i
                                    class="fa fa-eye"></i></a>
                        @endcan
                        @can('update-page')
                            <a href="{!! route('pages.setting', ['id'=>$page->get('id')]) !!}"
                               class='btn btn-info action-btn edit-btn'><i class="fas fa-cog"></i></a>
                        @endcan
                        @can('update-page')
                            <a href="{!! route('pages.edit', [$page->get('id')]) !!}"
                               class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                        @endcan
                        @can('delete-page')
                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("Are you sure want to delete this record ?")']) !!}
                        @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/select2.min.js') }}"></script>

<script>
    $(document).ready(function () {
        $.noConflict();
        $('#pages-table').DataTable();
    });
</script>
