<?php
$pageUrlParam = '';
if (isset($page)) {
    $pageUrlParam = '&page=' . phpb_e($page->getId());
    $pageTranslations = $page ? $page->getTranslations() : [];
}

?>

<div class="form-group col-sm-6 col-md-4">
    {!! Form::label('name', 'Name:') !!}
    @if(str_contains(url()->full(),'pages.setting?id'))
        {!! Form::text('name', $page->getName(), ['class' => 'form-control']) !!}
    @else
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    @endif
</div>

<div class="form-group col-sm-6 col-md-4">
    {!! Form::label('url', 'URL:') !!}
    @if(str_contains(url()->full(),'pages.setting?id'))
        {!! Form::text('url', $page->getRoute(), ['class' => 'form-control']) !!}
    @else
        {!! Form::text('url', null, ['class' => 'form-control']) !!}
    @endif
</div>

<div class="form-group col-sm-12">
    <?php
    foreach (phpb_active_languages() as $languageCode => $languageTranslation):
    ?>
    <h5 class="pt-2 border-bottom border-top"><?= phpb_trans('languages.' . $languageCode) ?></h5>
    <div class="pt-2 pl-3 pr-3">

    </div>

    <div class="row">
        <div class="col-sm-12">
            <h6 class="text-center">Web</h6>
        </div>

        <div class="form-group required col-sm-12">
            <label for="page-title"><?= phpb_trans('website-manager.page-title') ?></label>
            <input type="text" class="form-control" id="page-title" name="title[<?= phpb_e($languageCode) ?>]"
                   value="<?= phpb_e($pageTranslations[$languageCode]['title'] ?? '') ?>" required>
        </div>
        <div class="form-group col-sm-12">
            <label for="page-description">{{ __('pagebuilder.description') }}</label>
            <input type="text" class="form-control" id="page-description" name="description[<?= phpb_e($languageCode) ?>]"
                   value="<?= phpb_e($pageTranslations[$languageCode]['description'] ?? '') ?>">
        </div>

        <hr>

        <div class="col-sm-12">
            <h6 class="text-center">Twitter</h6>
        </div>

        <div class="form-group col-sm-6 col-md-4">
            <label for="page-twitter_creator">{{ __('pagebuilder.twitter_creator') }}</label>
            <input type="text" class="form-control" id="page-twitter_creator" name="twitter_creator[<?= phpb_e($languageCode) ?>]"
                   value="<?= phpb_e($pageTranslations[$languageCode]['twitter_creator'] ?? '') ?>">
        </div>
        <div class="form-group col-sm-6 col-md-4">
            <label for="page-twitter_card">{{ __('pagebuilder.twitter_card') }}</label>
            <input type="text" class="form-control" id="page-twitter_card" name="twitter_card[<?= phpb_e($languageCode) ?>]"
                   value="<?= phpb_e($pageTranslations[$languageCode]['twitter_card'] ?? '') ?>">
        </div>
        <div class="form-group col-sm-6 col-md-4">
            <label for="page-twitter_site">{{ __('pagebuilder.twitter_site') }}</label>
            <input type="text" class="form-control" id="page-twitter_site" name="twitter_site[<?= phpb_e($languageCode) ?>]"
                   value="<?= phpb_e($pageTranslations[$languageCode]['twitter_site'] ?? '') ?>">
        </div>
        <div class="form-group col-sm-6 col-md-4">
            <label for="page-twitter_title">{{ __('pagebuilder.twitter_title') }}</label>
            <input type="text" class="form-control" id="page-twitter_title" name="twitter_title[<?= phpb_e($languageCode) ?>]"
                   value="<?= phpb_e($pageTranslations[$languageCode]['twitter_title'] ?? '') ?>">
        </div>
        <div class="form-group col-sm-6 col-md-4">
            <label for="page-twitter_image">{{ __('pagebuilder.twitter_image') }}</label>
            <input type="text" class="form-control" id="page-twitter_image" name="twitter_image[<?= phpb_e($languageCode) ?>]"
                   value="<?= phpb_e($pageTranslations[$languageCode]['twitter_image'] ?? '') ?>">
        </div>
        <div class="form-group col-sm-6 col-md-4">
            <label for="page-twitter_image_alt">{{ __('pagebuilder.twitter_image_alt') }}</label>
            <input type="text" class="form-control" id="page-twitter_image_alt" name="twitter_image_alt[<?= phpb_e($languageCode) ?>]"
                   value="<?= phpb_e($pageTranslations[$languageCode]['twitter_image_alt'] ?? '') ?>">
        </div>
        <div class="form-group col-sm-12">
            <label for="page-twitter_description">{{ __('pagebuilder.twitter_description') }}</label>
            <input type="text" class="form-control" id="page-twitter_description" name="twitter_description[<?= phpb_e($languageCode) ?>]"
                   value="<?= phpb_e($pageTranslations[$languageCode]['twitter_description'] ?? '') ?>">
        </div>

        <div class="col-sm-12">
            <h6 class="text-center">Facebook</h6>
        </div>

        <div class="form-group col-sm-6 col-md-4">
            <label for="page-og_title">{{ __('pagebuilder.og_title') }}</label>
            <input type="text" class="form-control" id="page-og_title" name="og_title[<?= phpb_e($languageCode) ?>]"
                   value="<?= phpb_e($pageTranslations[$languageCode]['og_title'] ?? '') ?>">
        </div>
        <div class="form-group col-sm-6 col-md-4">
            <label for="page-og_type">{{ __('pagebuilder.og_type') }}</label>
            <input type="text" class="form-control" id="page-og_type" name="og_type[<?= phpb_e($languageCode) ?>]"
                   value="<?= phpb_e($pageTranslations[$languageCode]['og_type'] ?? '') ?>">
        </div>
        <div class="form-group col-sm-6 col-md-4">
            <label for="page-og_url">{{ __('pagebuilder.og_url') }}</label>
            <input type="text" class="form-control" id="page-og_url" name="og_url[<?= phpb_e($languageCode) ?>]"
                   value="<?= phpb_e($pageTranslations[$languageCode]['og_url'] ?? '') ?>">
        </div>
        <div class="form-group col-sm-6 col-md-4">
            <label for="page-og_image">{{ __('pagebuilder.og_image') }}</label>
            <input type="text" class="form-control" id="page-og_image" name="og_image[<?= phpb_e($languageCode) ?>]"
                   value="<?= phpb_e($pageTranslations[$languageCode]['og_image'] ?? '') ?>">
        </div>
        <div class="form-group col-sm-6 col-md-4">
            <label for="page-og_locale">{{ __('pagebuilder.og_locale') }}</label>
            <input type="text" class="form-control" id="page-og_locale" name="og_locale[<?= phpb_e($languageCode) ?>]"
                   value="<?= phpb_e($pageTranslations[$languageCode]['og_locale'] ?? '') ?>">
        </div>
        <div class="form-group col-sm-6 col-md-4">
            <label for="page-fb_app_id">{{ __('pagebuilder.fb_app_id') }}</label>
            <input type="text" class="form-control" id="page-fb_app_id" name="fb_app_id[<?= phpb_e($languageCode) ?>]"
                   value="<?= phpb_e($pageTranslations[$languageCode]['fb_app_id'] ?? '') ?>">
        </div>
        <div class="form-group col-sm-12">
            <label for="page-og_description">{{ __('pagebuilder.og_description') }}</label>
            <input type="text" class="form-control" id="page-og_description" name="og_description[<?= phpb_e($languageCode) ?>]"
                   value="<?= phpb_e($pageTranslations[$languageCode]['og_description'] ?? '') ?>">
        </div>
    </div>


    <?php
    endforeach;
    ?>
</div>

@can('update-page-status')
    <div class="form-group col-sm-4">
        {!! Form::label('status', 'Status:') !!}
        {!! Form::select('status', $status, null, ['class' => 'form-control select2']) !!}
    </div>
@endcan

@can('update-published-date')
    <div class="form-group col-sm-4">
        {!! Form::label('published_at', 'Published At:') !!}
        @if (!str_contains(url()->full(),'pages.setting?id'))
            <input type="datetime-local" class="form-control" value="{{date('Y-m-d\TH:i')}}" id="published_at"
                   name="published_at">
        @else
            <input type="datetime-local" class="form-control"
                   value="{{date('Y-m-d\TH:i',strtotime($page->published_at))}}" id="published_at" name="published_at">
        @endif
    </div>
@endcan

@can('update-unpublished-date')
    <div class="form-group col-sm-4">
        {!! Form::label('unpublished_at', 'Unpublished At:') !!}
        @if (!str_contains(url()->full(),'pages.setting?id'))
            <input type="datetime-local" class="form-control"
                   value="{{date('Y-m-d\TH:i',strtotime('+1 month',strtotime(date('Y-m-d\TH:i'))))}}"
                   id="unpublished_at"
                   name="unpublished_at">
        @else
            <input type="datetime-local" class="form-control"
                   value="{{date('Y-m-d\TH:i',strtotime($page->unpublished_at))}}" id="unpublished_at"
                   name="unpublished_at">
        @endif
    </div>
@endcan

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('pages.index') }}" class="btn btn-light">Cancel</a>
</div>
