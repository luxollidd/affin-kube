<li class="side-menus {{ Request::is('*home') ? 'active' : '' }}">
    <a class="nav-link" href="/home">
        <i class="fas fa-desktop"></i><span>Dashboard</span>
    </a>
</li>

@can('read-user')
    <li class="side-menus {{ Request::is('users*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('users.index') }}"><i class="fas fa-users"></i><span>Users</span></a>
    </li>
@endcan

@can('read-role')
    <li class="side-menus {{ Request::is('roles*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('roles.index') }}"><i class="fas fa-user-tag"></i><span>Roles</span></a>
    </li>
@endcan

@can('read-page')
    <li class="side-menus {{ Request::is('pages*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('pages.index') }}"><i class="fas fa-file"></i><span>Pages</span></a>
    </li>
@endcan

<li class="side-menus {{ Request::is('settings*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('settings.edit') }}"><i class="fas fa-file"></i><span>Settings</span></a>
</li>
