@extends('layouts.app')
@section('title')
    User Details
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>User Details</h1>
            <div class="section-header-breadcrumb">
                @can('update-user')
                    <a href="{!! route('users.edit', [$user->id]) !!}"
                       class='btn btn-primary form-btn mr-2'>Edit</a>
                @endcan
                <a href="{{ route('users.index') }}"
                   class="btn btn-primary form-btn float-right">Back</a>
            </div>
        </div>
        @include('stisla-templates::common.errors')
        <div class="section-body">
            <div style="font-style: italic;font-size: smaller;" class="d-flex flex-row justify-content-end">
                <div class="form-group col-md-3">
                    {!! Form::label('created_at', 'Created At:') !!}
                    <span>{{ $user->created_at }}</span><br>
                    {!! Form::label('updated_at', 'Updated At:') !!}
                    <span>{{ $user->updated_at }}</span>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    @include('users.show_fields')
                </div>
            </div>
        </div>
    </section>
@endsection
