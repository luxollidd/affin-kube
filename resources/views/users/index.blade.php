@extends('layouts.app')
@section('title')
    Users
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Users</h1>
            <div class="section-header-breadcrumb">
                @can('create-user')
                    <a href="{{ route('users.create')}}" class="btn btn-primary form-btn">User <i
                            class="fas fa-plus"></i></a>
                @endcan
            </div>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    @include('users.table')
                </div>
            </div>
        </div>

        {{--        @include('stisla-templates::common.paginate', ['records' => $users])--}}

    </section>
@endsection

