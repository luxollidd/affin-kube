<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('confirm_password', 'Confirm Password:') !!}
    {!! Form::password('confirm_password', ['class' => 'form-control']) !!}
</div>

@can('assign-role')
    <div class="form-group col-sm-12">
        {!! Form::label('role', 'Role:') !!}
        <select name="role" id="role" class="form-control select2">
            @foreach ($roles as $role)
                <option
                    @if(Request::path() != 'users/create')
                    @if($role['name']==$user->roles->first()['name'])
                    selected
                    @endif
                    @endif
                    value="{{$role['name']}}">{{$role['name']}}</option>
            @endforeach
        </select>
    </div>
@endcan

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('users.index') }}" class="btn btn-light">Cancel</a>
</div>
