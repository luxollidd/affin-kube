<link href="{{ asset('/assets/css/jquery.dataTables.min.css') }}" rel="stylesheet">

<div class="table-responsive">
    <table class="table table-bordered" id="users-table">
        <thead>
        <tr>
            <th class="text-center">Name</th>
            <th class="text-center">Email</th>
            <th class="text-center">Role</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td class="text-center">{{ $user->name }}</td>
                <td class="text-center">{{ $user->email }}</td>
                <td class="text-center text-capitalize">{{ $user->roles->first()['name'] ?? 'No Role' }}</td>
                <td class="text-center">
                    {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('read-user')
                            <a href="{!! route('users.show', [$user->id]) !!}" class='btn btn-light action-btn '><i
                                    class="fa fa-eye"></i></a>
                        @endcan
                        @can('update-user')
                            <a href="{!! route('users.edit', [$user->id]) !!}"
                               class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                        @endcan
                        @can('delete-user')
                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("Are you sure want to delete this record ?")']) !!}
                        @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/select2.min.js') }}"></script>

<script>
    $(document).ready(function () {
        $.noConflict();
        $('#users-table').DataTable();
    });
</script>
