<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <span>{{ $user->name }}</span>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <span>{{ $user->email }}</span>
</div>

<div class="form-group">
    {!! Form::label('role', 'Role:') !!}
    <span class="text-capitalize">{{ $user->roles->first()['name'] }}</span>
</div>

