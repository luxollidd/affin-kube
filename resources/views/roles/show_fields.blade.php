<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <span class="text-capitalize">{{ $role->name }}</span>
</div>

<!-- Guard Name Field -->
<div class="form-group">
    {!! Form::label('guard_name', 'Guard Name:') !!}
    <span class="text-capitalize">{{ $role->guard_name }}</span>
</div>

<div class="form-group">
    {!! Form::label('permissions', 'Permissions:') !!}
    <ul>
        @foreach($permissions as $permission)
            <li>{{ $permission->name }}</li>
        @endforeach
    </ul>
</div>

