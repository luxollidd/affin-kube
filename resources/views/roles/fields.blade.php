<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Guard Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('guard_name', 'Guard Name:') !!}
    {!! Form::text('guard_name', 'web', ['class' => 'form-control', 'readonly']) !!}
</div>

<div class="form-group col-sm-9">
    {!! Form::label('permissions', 'Permissions:') !!}
    <select name="permissions[]" id="permissions" class="form-control select2" multiple="">
        @foreach ($permissions as $permission)
            <option
                @if(Request::path() != 'roles/create')
                @foreach($role_permissions as $role_permission)
                @if($permission['name']==$role_permission->name)
                selected
                @endif
                @endforeach
                @endif
                value="{{$permission['name']}}">{{$permission['name']}}</option>
        @endforeach
    </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('roles.index') }}" class="btn btn-light">Cancel</a>
</div>
