<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');

Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');

Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');

Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');

Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');

Route::post(
    'generator_builder/generate-from-file',
    '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
)->name('io_generator_builder_generate_from_file');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('roles', App\Http\Controllers\RoleController::class);
    Route::resource('users', App\Http\Controllers\UserController::class);
    Route::resource('pages', App\Http\Controllers\PageBuilderController::class);
    Route::get('admin/settings', 'App\Http\Controllers\SettingsController@edit')->name('settings.edit');
    Route::post('admin/settings', 'App\Http\Controllers\SettingsController@update')->name('settings.update');
    Route::get('pages.setting', 'App\Http\Controllers\PageBuilderController@setting')->name('pages.setting');
    Route::post('admin/pagebuilder', 'App\Http\Controllers\PageBuilderController@storePage');
    Route::patch('admin/pagebuilder', 'App\Http\Controllers\PageBuilderController@storePage');
    Route::any('preview/{uri}', [
        'uses' => 'App\Http\Controllers\RouteController@previewPage',
        'as' => 'page',
    ])->where('uri', '.*');
});

// TODO: temporary workaround to get the public theme file
Route::get('themes/demo/{prefix}/{name}.{extension}', 'App\Http\Controllers\RouteController@getThemePublicFile')
    ->where('name', '[a-zA-Z0-9-]+')
    ->where('prefix', '[js, css]+')
    ->where('extension', '[js, css]+');

Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'App\Http\Controllers\LanguageController@switchLang']);
Route::get('currentLang', ['as' => 'lang.get', 'uses' => 'App\Http\Controllers\LanguageController@getCurrentLang']);

Route::any('{uri}', [
    'uses' => 'App\Http\Controllers\RouteController@indexHtml',
    'as' => 'page',
])->where('uri', '.*');
