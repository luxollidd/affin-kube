
#!/usr/bin/env sh
set -ex
# IMAGE=laravel,TAG=v1 ./
docker build -t luxollidd/affin-$IMAGE:$TAG . -f Dockerfile.$IMAGE
docker push luxollidd/affin-$IMAGE:$TAG