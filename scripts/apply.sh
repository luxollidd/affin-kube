#!/usr/bin/env sh

kubectl apply -f configmaps
kubectl apply -f services
kubectl apply -f ingress
kubectl apply -f deployments