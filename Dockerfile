FROM node:12-buster-slim

WORKDIR /app

COPY package.json package-lock.json ./

RUN apt update && \
    apt install -y \
        build-essential \
        libpng-dev && \
    npm ci && \
    npm rebuild

COPY artisan webpack.mix.js ./
COPY resources/assets resources/assets

RUN npm run production


FROM php:7.4-fpm

COPY --from=composer:1 /usr/bin/composer /usr/bin/composer
COPY --from=mlocati/php-extension-installer:latest /usr/bin/install-php-extensions /usr/bin/

RUN apt update && \
    apt install --yes \
        unzip

WORKDIR /app

COPY composer.json composer.lock ./

RUN composer install --no-autoloader --no-scripts


FROM php:7.4-fpm

COPY --from=composer:1 /usr/bin/composer /usr/bin/composer
COPY --from=mlocati/php-extension-installer:latest /usr/bin/install-php-extensions /usr/bin/

RUN sed -i "s| = www-data| = root|g" /usr/local/etc/php-fpm.d/www.conf && \
    sed -i "s| = www-data| = root|g" /usr/local/etc/php-fpm.d/www.conf && \
    install-php-extensions \
        opcache \
        pdo_mysql && \
    apt update && \
    apt install --yes \
        nginx && \
    rm --force --recursive /var/lib/apt/lists/*

WORKDIR /app

COPY . .

COPY --from=0 /app/public public
COPY --from=1 /app/vendor vendor

# We must exclude config/pagebuilder.php before composer dump-autoload, or else it'll make db connection
RUN mv config/pagebuilder.php config/pagebuilder.php.bak && \
    composer dump-autoload --optimize && \
    mv config/pagebuilder.php.bak config/pagebuilder.php && \
    chmod +x start.sh
    #php artisan route:cache

CMD ["./start.sh"]

EXPOSE 80
