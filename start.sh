#!/usr/bin/env sh
nginx -g 'daemon off;' &
php-fpm --allow-to-run-as-root &
tail --follow=name --retry src/storage/logs/laravel.log &
wait
