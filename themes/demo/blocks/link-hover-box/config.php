<?php

return [
    'title' => 'Link Hover Box',
    'category' => 'Advanced',
    'icon' => 'fa fa-link',
    "settings" => [
        "box-title-1" => [
            'label' => 'title-1',
            'type' => 'text',
            'value' => 'Personal'
        ],
        "image-1" => [
            'label' => 'image-1',
            'type' => 'text',
            'value' => '/img/1.png'
        ],
        "box-title-2" => [
            'label' => 'title-2',
            'type' => 'text',
            'value' => 'Professional'
        ],
        "image-2" => [
            'label' => 'image-2',
            'type' => 'text',
            'value' => '/img/2.png'
        ],
        "box-title-3" => [
            'label' => 'title-3',
            'type' => 'text',
            'value' => 'SME'
        ],
        "image-3" => [
            'label' => 'image-3',
            'type' => 'text',
            'value' => '/img/3.png'
        ],
        "box-title-4" => [
            'label' => 'title-4',
            'type' => 'text',
            'value' => 'Corporate'
        ],
        "image-4" => [
            'label' => 'image-4',
            'type' => 'text',
            'value' => '/img/4.png'
        ],
    ],
];
