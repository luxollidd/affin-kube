<style>
    .flex-container {
        display: flex;
        flex-wrap: nowrap;
    }
</style>

<section class="block block-link-hover-box">
    <div class="block-container container">
        <div>
            <h1 class="text-center text-muted p-5">Discover what you need</h1>
        </div>
        <div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 m-b30 fadeInUp">
                    <div class="dez-box team-bx">
                        <div class="dez-media">
                            <img src="<?= $block->setting('image-1') ?>">
                        </div>
                        <div class="dez-title-bx">
                            <h2 class="m-b0"><?= $block->setting('box-title-1') ?></h2>
                        </div>
                        <div class="dez-info">
                            <div class="text-left">
                                <h3 class="dez-name m-b10"><a href="#" class="link-text">Personal</a></h3><hr>
                                <p class="m-0"><a href="#" class="link-text">MyDeposits</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">MyCards</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">MyLoans&Financing</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">MyProtections</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">MyInvestments</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">Promotions</a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 m-b30 fadeInUp">
                    <div class="dez-box team-bx">
                        <div class="dez-media">
                            <img src="<?= $block->setting('image-2') ?>">
                        </div>
                        <div class="dez-title-bx">
                            <h2 class="m-b0"><?= $block->setting('box-title-2') ?></h2>
                        </div>
                        <div class="dez-info">
                            <div class="text-left">
                                <h3 class="dez-name m-b10"><a href="#" class="link-text">Professional</a></h3><hr>
                                <p class="m-0"><a href="#" class="link-text">Link1</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">Link2</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">Link3</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">Link4</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">Link5</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">Link6</a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 m-b30 fadeInUp">
                    <div class="dez-box team-bx">
                        <div class="dez-media">
                            <img src="<?= $block->setting('image-3') ?>">
                        </div>
                        <div class="dez-title-bx">
                            <h2 class="m-b0"><?= $block->setting('box-title-3') ?></h2>
                        </div>
                        <div class="dez-info">
                            <div class="text-left">
                                <h3 class="dez-name m-b10"><a href="#" class="link-text">SME</a></h3><hr>
                                <p class="m-0"><a href="#" class="link-text">Link1</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">Link2</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">Link3</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">Link4</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">Link5</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">Link6</a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 m-b30 fadeInUp">
                    <div class="dez-box team-bx">
                        <div class="dez-media">
                            <img src="<?= $block->setting('image-4') ?>">
                        </div>
                        <div class="dez-title-bx">
                            <h2 class="m-b0"><?= $block->setting('box-title-4') ?></h2>
                        </div>
                        <div class="dez-info">
                            <div class="text-left">
                                <h3 class="dez-name m-b10"><a href="#" class="link-text">Corporate</a></h3><hr>
                                <p class="m-0"><a href="#" class="link-text">Link1</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">Link2</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">Link3</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">Link4</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">Link5</a></p><hr>
                                <p class="m-0"><a href="#" class="link-text">Link6</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
