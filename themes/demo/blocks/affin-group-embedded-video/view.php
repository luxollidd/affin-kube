<style>
    .block-affin-group-embedded-video .full-width-video {
        width: 100%;
    }

    .block-affin-group-embedded-video video {
        height: unset !important;
    }
</style>

<section class="block block-affin-group-embedded-video">
    <div class="position-relative">
        <video class="full-width-video"
               src="<?= $block->setting('video') ?>" <?= $block->setting('autoplay') ?> <?= $block->setting('loop') ?> <?= $block->setting('muted') ?>></video>
        <div class="container">
            [block slug="affin-group-embedded-video-text"]
        </div>
    </div>
</section>
