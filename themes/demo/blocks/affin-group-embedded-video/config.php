<?php

return [
    'title' => 'Affin Group Embedded Video',
    'category' => 'Affin Group',
    'icon' => 'fa fa-video-camera',
    "settings" => [
        "video" => [
            'label' => 'video',
            'type' => 'text',
            'value' => 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4'
        ],
        "muted" => [
            'label' => 'muted?',
            'type' => 'select',
            'options' => [
                ['id' => 'muted', 'name' => 'Muted'],
                ['id' => ' ', 'name' => 'Not muted'],
            ],
            'value' => 'muted'
        ],
        "loop" => [
            'label' => 'loop?',
            'type' => 'select',
            'options' => [
                ['id' => 'loop', 'name' => 'Loop'],
                ['id' => ' ', 'name' => 'Not Loop'],
            ],
            'value' => 'loop'
        ],
        "autoplay" => [
            'label' => 'autoplay?',
            'type' => 'select',
            'options' => [
                ['id' => 'autoplay', 'name' => 'Autoplay'],
                ['id' => ' ', 'name' => 'Not Autoplay'],
            ],
            'value' => 'autoplay'
        ],
    ],
];
