<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title><?= $page->getTranslation('title') ?></title>
    <meta name="description" value="<?= $page->getTranslation('description') ?>">
    <link rel="stylesheet" href="<?= phpb_theme_asset('css/style.css') ?>" />

    <!-- Custom Meta Tags For Twitter -->
    <meta name="twitter:card" content="<?= $page->getTranslation("twitter_card") ?>">
    <meta name="twitter:site" content="<?= $page->getTranslation("twitter_site") ?>">
    <meta name="twitter:creator" content="<?= $page->getTranslation("twitter_creator") ?>">
    <meta name="twitter:title" content="<?= $page->getTranslation("twitter_title") ?>">
    <meta name="twitter:description" content="<?= $page->getTranslation("twitter_description") ?>">
    <meta name="twitter:image" content="<?= $page->getTranslation("twitter_image") ?>">
    <meta name="twitter:image:alt" content="<?= $page->getTranslation("twitter_image_alt") ?>">

    <!-- Custom Meta Tags For Facebook -->
    <meta name="og:type" content="<?= $page->getTranslation("twitter_card") ?>">
    <meta name="og:url" content="<?= $page->getTranslation("twitter_site") ?>">
    <meta name="og:locale" content="<?= $page->getTranslation("twitter_creator") ?>">
    <meta name="og:title" content="<?= $page->getTranslation("twitter_title") ?>">
    <meta name="og:description" content="<?= $page->getTranslation("twitter_description") ?>">
    <meta name="og:image" content="<?= $page->getTranslation("twitter_image") ?>">
    <meta name="fb:app_id" content="<?= $page->getTranslation("twitter_image_alt") ?>">
</head>
<body>
<li><a href="/lang/en">English</a></li>
<li><a href="/lang/nl">Dutch</a></li>

<h1>Secondary Layout</h1>

<?= $body ?>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
