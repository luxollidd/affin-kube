/* Affin Always Slider */
var slideIndex = 1;
showSlides(slideIndex);

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("my-slide");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    if (slides[slideIndex - 1] !== undefined) {
        slides[slideIndex - 1].style.display = "block";
    }
    if (dots[slideIndex - 1] !== undefined) {
        dots[slideIndex - 1].className += " active";
    }
}

/* Affin Always Slider */

/* Affin Group Slider */
showSlide(slideIndex);

function currentSlides(n) {
    showSlide(slideIndex = n);
}

function showSlide(n) {
    var i;
    var slides = document.getElementsByClassName("slide");
    var dots = document.getElementsByClassName("my-dot");
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    if (slides[slideIndex - 1] !== undefined) {
        slides[slideIndex - 1].style.display = "block";
    }
    if (dots[slideIndex - 1] !== undefined) {
        dots[slideIndex - 1].className += " active";
    }
}

/* Affin Group Slider */

/* A1addin Navbar */
var button = document.getElementById('icon');
var x = document.getElementById("myTopnav");
if (button) {
    button.addEventListener('click', function () {
        if (x.className === "topnav") {
            x.className += " responsive";
            button.className += ' active-close';
        } else {
            x.className = "topnav";
            button.className = 'icon pt-4';
        }
    });
}
/* A1addin Navbar */

/* Affin Always Range */
if (document.getElementById("salary")) {
    document.getElementById("salary").value = 3000;
    drag();
}

function drag() {
    if (document.getElementById("salary").value !== null) {
        var x = document.getElementById("salary").value;
        document.getElementById("result").innerHTML = 'My salary is RM' + x;
    }
}

/* Affin Always Range */

/* Affin Always Navbar */
/* Modal */
var modal = document.getElementById("myModal");
var btn = document.getElementById("myBtn");
var span = document.getElementsByClassName("close")[0];

if (btn) {
    btn.onclick = function () {
        modal.style.display = "block";
    }
}

if (span) {
    span.onclick = function () {
        modal.style.display = "none";
    }
}

window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

/* Modal */

function openNav() {
    document.getElementById("myNav").style.height = "100%";
}

function closeNav() {
    document.getElementById("myNav").style.height = "0%";
}

/* Affin Always Navbar */

/* Affin Always OR A1addin Tab With Image */
changeTab('tab-1');

function changeTab(tabName) {
    var x = document.getElementsByClassName("tab");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }

    if (!document.getElementById(tabName)) return;
    document.getElementById(tabName).style.display = "block";

    switch (tabName) {
        case 'tab-1':
            toggleTabStyle(x.length, 1);
            break;
        case 'tab-2':
            toggleTabStyle(x.length, 2);
            break;
        case 'tab-3':
            toggleTabStyle(x.length, 3);
            break;
    }
}

function toggleTabStyle(lengthOfTabs, toggleTab) {
    for (i = 1; i <= lengthOfTabs; i++) {
        if (i === toggleTab) {
            document.getElementById('tab-link-' + i).classList.add('active');
        } else {
            document.getElementById('tab-link-' + i).classList.remove('active');
        }
    }
}

/* Affin Always OR A1addin Tab With Image */
