<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PageInfo extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = 'page_info';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'page_id',
        'route',
        'status',
        'published_at',
        'unpublished_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];
}
