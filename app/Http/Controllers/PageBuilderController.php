<?php

namespace App\Http\Controllers;

use App\Models\PageInfo;
use App\Repositories\PageRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use PHPageBuilder\Contracts\PageContract;

class PageBuilderController extends Controller
{
    public function index()
    {
        $pageRepository = new PageRepository;
        $pages = $pageRepository->getAll();

        return view('pages.index')
            ->with('pages', $pages);
    }

    public function create()
    {
        $status = config('constant.STATUS');
        return view('pages.create')
            ->with('status', $status);
    }

    public function store(Request $request)
    {
        $pageRepository = new PageRepository;

        $input = $request->all();
        $input['layout'] = 'master';
        $input['url'] = '/' . $input['url'];
        $input['route'] = [];

        foreach ($input['title'] as $languageCode => $translation) {
            $input['route'][$languageCode] = $input['url'];
        }

        $page = $pageRepository->create($input);

        $input['page_id'] = $page->getId();
        $input['route'] = $input['url'];

        $page_info = PageInfo::create($input);

        return redirect(route('pages.index'));
    }

    public function show($pageId)
    {
        $pageId = is_numeric($pageId) ? $pageId : ($_GET['page'] ?? null);
        $pageRepository = new PageRepository;
        $page = $pageRepository->findWithId($pageId);

        return redirect(phpb_e(phpb_full_url('/preview' . $page->getRoute())));
    }

    public function setting(Request $request)
    {
        $pageRepository = new PageRepository;
        $page = $pageRepository->findWithId($request['id']);
        $status = config('constant.STATUS');
        $pageInfo = PageInfo::where('page_id', '=', $request['id'])->first();
        if ($pageInfo) {
            $page->status = $pageInfo->status;
            $page->published_at = $pageInfo->published_at;
            $page->unpublished_at = $pageInfo->unpublished_at;
        }

        return view('pages.edit')
            ->with('status', $status)
            ->with('page', $page);
    }

    public function edit($pageId = null)
    {
        $route = $_GET['route'] ?? null;
        $action = $_GET['action'] ?? null;
        $pageId = is_numeric($pageId) ? $pageId : ($_GET['page'] ?? null);
        $pageRepository = new PageRepository;
        $page = $pageRepository->findWithId($pageId);

        $phpPageBuilder = app()->make('phpPageBuilder');
        $pageBuilder = $phpPageBuilder->getPageBuilder();
        $pageBuilder->handleRequest($route, $action, $page);
    }

    public function storePage(Request $request, $pageId = null)
    {
        $action = $request->get('action');
        $pageId = $request->get('page');
        $pageRepository = new PageRepository;
        $page = $pageRepository->findWithId($pageId);


        if (!($page instanceof PageContract)) {
            return false;
        }

        $phpPageBuilder = app()->make('phpPageBuilder');
        $pageBuilder = $phpPageBuilder->getPageBuilder();
        $data = json_decode($request->get('data'), true);

        switch ($action) {
            case 'store':
                $pageBuilder->updatePage($page, $data);
                $pageBuilder->saveAllAsHtml($page);
                break;
            case 'renderLanguageVariant':
                $language = $request->get('language');
                $pageBuilder->renderLanguageVariant($page, $language, $data, true);
                break;
            case 'renderBlock':
                $language = $request->get('language');
                $pageBuilder->renderPageBuilderBlock($page, $language, $data, true);
                break;
            case 'upload':
                $pageBuilder->handleFileUpload();
                break;
            case 'upload_delete':
                $pageBuilder->handleFileDelete();
                break;

        }
    }


    public function update(Request $request, $pageId = null)
    {
        $input = $request->all();
        $input['layout'] = 'master';
        $input['route'] = [];

        foreach ($input['title'] as $languageCode => $translation) {
            $input['route'][$languageCode] = $input['url'];
        }

        $pageRepository = new PageRepository;
        $page = $pageRepository->findWithId($pageId);
        $pageRepository->update($page, $input);

        $page_info = PageInfo::where('page_id', '=', $pageId)->first();

        $input['route'] = $input['url'];

        $page_info->update($input);

        return redirect(route('pages.index'));
    }

    public function destroy($pageId)
    {
        $pageRepository = new PageRepository;
        $page = $pageRepository->destroy($pageId);

        return redirect(route('pages.index'));
    }
}
