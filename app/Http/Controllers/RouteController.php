<?php

namespace App\Http\Controllers;

use App\Models\PageInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Lang;
use PHPageBuilder\Contracts\RouterContract;
use PHPageBuilder\Modules\GrapesJS\PageRenderer;
use PHPageBuilder\Repositories\PageRepository;
use PHPageBuilder\Theme;

class RouteController extends Controller
{
    /**
     * @var RouterContract $router
     */
    protected $router;

    public function __construct()
    {
        $this->router = phpb_instance('router');
    }

    public function index(Request $request)
    {
        $pageTranslation = $this->router->resolve(phpb_current_relative_url());
        $pageId = $pageTranslation->page_id;
        $theme = new Theme(config('pagebuilder.theme'), config('pagebuilder.theme.active_theme'));
        $page = (new PageRepository)->findWithId($pageId);
        $currentDomain = $request->getSchemeAndHttpHost();

        switch ($currentDomain) {
            case 'http://localhost:8000':
                $page->layout = 'master';
                break;
            case 'http://localhost:8001':
                $page->layout = 'secondary';
                break;
            default:
                $page->layout = 'master';
        }

        $pageRenderer = new PageRenderer($theme, $page);

        $currentLanguage = $request->server('HTTP_ACCEPT_LANGUAGE');
        $pageRenderer->setLanguage($currentLanguage);

        $html = $pageRenderer->render();

        return $html;
    }

    public function indexHtml(Request $request)
    {
        $pageTranslation = $this->router->resolve(phpb_current_relative_url());
        $slug = explode('/', $pageTranslation->route)[1];
        $currentDomain = $request->getHttpHost();
        $file = File::get(public_path() . '/html/' . $currentDomain . '/' . Lang::locale() . '/' . $slug . '.html');

        $pageInfo = PageInfo::where('page_id', '=', $pageTranslation->page_id)->first();

        if ($pageInfo->status !== 'approved' || $pageInfo->published_at >= date('Y-m-d H:i:s') || $pageInfo->unpublished_at <= date('Y-m-d H:i:s')) {
            abort(404);
        }

        return $file;
    }

    public function previewPage(Request $request)
    {
        $pageTranslation = $this->router->resolve(substr(phpb_current_relative_url(), 8));
        $slug = explode('/', $pageTranslation->route)[1];
        $currentDomain = $request->getHttpHost();
        $filepath = public_path() . '/html/' . $currentDomain . '/' . Lang::locale() . '/' . $slug . '.html';
        if (file_exists($filepath)) {
            $file = File::get($filepath);
        } else {
            abort(404);
        }

        return $file;
    }

    public function getThemePublicFile($prefix, $name, $extension)
    {
        $path = base_path('themes/demo/public/' . $prefix . '/' . $name . '.' . $extension);
        $contentType = null;

        switch ($extension) {
            case 'css':
                $contentType = 'text/css';
                break;
            case 'js':
                $contentType = 'application/javascript';
                break;
        }
        $headers = ['Content-Type' => $contentType];


        if (file_exists($path)) {
            return response()->file($path, $headers);
        }

        abort(404);
    }
}
