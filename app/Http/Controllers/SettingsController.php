<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPageBuilder\Repositories\SettingRepository;

class SettingsController extends Controller
{
    public function edit()
    {
        return view('settings');
    }

    public function update(Request $request)
    {
        $settingRepository = new SettingRepository;
        $success = $settingRepository->updateSettings($request->except('_token'));

        return redirect(route('pages.index'));
    }
}
