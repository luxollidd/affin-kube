<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use ReflectionClass;
use Illuminate\Filesystem\Filesystem;

class GetSQLQuery extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:sql';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $files;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $migrator = app('migrator');
        $db = $migrator->resolveConnection(null);
        $migrations = $migrator->getMigrationFiles('database/migrations');
        $migrator->requireFiles($migrations);
        $queries = [];

        foreach($migrations as $migration) {
            $migration_name = $migration;
            $migration = $this->resolvePath($migration);

            $queries[] = [
                'name' => $migration_name,
                'queries' => array_column($db->pretend(function() use ($migration) { $migration->up(); }), 'query'),
            ];
        }

        dd($queries);
    }

    protected function resolvePath(string $path)
    {
        $class = $this->getMigrationClass($this->getMigrationName($path));

        if (class_exists($class) && realpath($path) == (new ReflectionClass($class))->getFileName()) {
            return new $class;
        }

        $migration = $this->files->getRequire($path);

        return is_object($migration) ? $migration : new $class;
    }

    protected function getMigrationClass(string $migrationName): string
    {
        return Str::studly(implode('_', array_slice(explode('_', $migrationName), 4)));
    }

    public function getMigrationName($path)
    {
        return str_replace('.php', '', basename($path));
    }
}
