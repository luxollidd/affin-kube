
#!/usr/bin/env sh
set -ex
# IMAGE=laravel,TAG=v1 ./
docker build -t luxollidd/affincms:v1 . -f Dockerfile
docker push luxollidd/affincms:v1